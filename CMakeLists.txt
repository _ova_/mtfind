cmake_minimum_required(VERSION 3.10)

project(mtfind)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(LIBS
  pthread
)

set(INC_DIRS "${INC_DIRS}"
  finder/
)

add_executable(mtfind
  main.cc
  finder/finder.cc
)

include_directories(mtfind ${INC_DIRS})
target_link_libraries(mtfind ${LIBS})
