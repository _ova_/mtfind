#include "finder.h"

namespace finder {

Finder::Finder(std::shared_ptr<std::istream> input_stream) {
  if (input_stream->good()) {
    while (!input_stream->eof()) {
      data_.push_back("");
      std::getline(*input_stream, data_.back());
    }
  }
}

Finder::~Finder() {
  for (auto& t : threads_) {
    if (t.joinable()) {
      t.join();
    }
  }
}

std::map<size_t, Result> Finder::find(Pattern const& pattern) {
  std::map<std::string, Result> filtered_res{};
  last_readed_index_ = 0;

  auto finder = [this](std::map<std::string, Result>& res, Pattern const& pat) {
    auto next_str = nextString();
    while (next_str.line_number != 0) {
      auto result = findInStr(next_str, pat);
      if (result.line_num != 0) {
        const std::lock_guard<std::mutex> lock(m_);
        res.emplace(std::make_pair(result.result, result));
      }
      next_str = nextString();
    }
  };

  auto const proc_count = std::thread::hardware_concurrency();
  for (size_t i = 0; i < proc_count; ++i) {
    threads_.push_back(std::thread(finder, std::ref(filtered_res), pattern));
  }

  for (auto& t : threads_) {
    if (t.joinable()) {
      t.join();
    }
  }

  std::map<size_t, Result> sorted_res{};
  for (auto const& e : filtered_res) {
    sorted_res.emplace(e.second.line_num, e.second);
  }

  return sorted_res;
}

Result Finder::findInStr(IndexedString const& src_str, Pattern const& pattern) {
  if (src_str.str.length() < pattern.str.length()) {
    return {};
  }

  auto ch = src_str.str.cbegin() + pattern.starting_margin;
  auto p_ch = pattern.str.cbegin() + pattern.starting_margin;
  bool found{false};
  auto pos{src_str.str.cend()};

  while ((ch != src_str.str.cend()) && (p_ch != pattern.str.cend())) {
    if (*p_ch == kAnySymbol) {
      found = true;
    } else {
      if (*ch == *p_ch) {
        if (pos == src_str.str.cend()) {
          found = true;
          pos = ch - pattern.starting_margin;
        }
      } else {
        found = false;
      }
    }
    if (found) {
      ++p_ch;
    } else {
      pos = src_str.str.cend();
      p_ch = pattern.str.cbegin() + pattern.starting_margin;
    }
    ++ch;
  }

  if (found && !(*p_ch)) {
    return {.line_num = src_str.line_number,
            .pos = static_cast<size_t>(pos - src_str.str.cbegin()),
            .result = std::string(ch - pattern.str.length(), pattern.str.length())};
  }
  return {};
}

Finder::IndexedString Finder::nextString() {
  IndexedString result{};
  std::lock_guard<std::mutex> lock(m_);
  if (last_readed_index_ < data_.size()) {
    result = {.line_number = last_readed_index_ + 1, .str = data_.at(last_readed_index_)};
    ++last_readed_index_;
  }
  return result;
}

} // namespace finder
