#ifndef FINDER_H
#define FINDER_H

#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

namespace finder {

char const kAnySymbol{'?'};

struct Result {
  size_t line_num{};
  size_t pos{};
  std::string result{};
};

struct Pattern {
  Pattern(std::string_view const s) : str{s} {
    for (auto const& ch : s) {
      static bool margin_found{};
      if (ch == '\n') {
        str = {};
        starting_margin = 0;
        break;
      } else if (!margin_found && ch == kAnySymbol) {
        ++starting_margin;
      } else if (ch != kAnySymbol) {
        margin_found = true;
      }
    }
  }
  std::string_view str{};
  size_t starting_margin{};
};

class Finder {
  private:
    struct IndexedString {
      size_t line_number{};
      std::string_view str{};
    };

  public:
    Finder(std::shared_ptr<std::istream> input_stream);
    ~Finder();

    std::map<size_t, Result> find(Pattern const& pattern);

  private:
    void readStream(std::shared_ptr<std::istream> stream);
    Result findInStr(IndexedString const& src_str, Pattern const& pattern);
    IndexedString nextString();

  private:
    std::mutex m_{};
    std::vector<std::string> data_{};
    size_t last_readed_index_{};
    std::vector<std::thread> threads_{};
};

} //namespace finder

#endif // FINDER_H