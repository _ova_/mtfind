#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string_view>

#include "finder/finder.h"

void exitFailure(std::string_view const message) {
  std::cout << message << std::endl;
  exit(EXIT_FAILURE);
}

int main(int argc, char** argv) {
  if (argc != 3) {
    exitFailure("the program expect 2 arguments:\n\t1 - text file name\n\t2 - test mask");
  }

  std::string_view pat{argv[2]};
  if (pat.length() < 1 || pat.length() > 100) {
    exitFailure("the pattern must contain at least one and less then 100 symbols");
  }
  finder::Pattern pattern{pat};
  if (pattern.str.length() == 0) {
    exitFailure("Probably, the pattern contains newline character. Remove it and try again.");
  }

  auto file_stream = std::make_shared<std::ifstream>(argv[1]);
  if (!file_stream->is_open()) {
    exitFailure(std::string("file '") + argv[1] + "' can not be opened");
  }

  finder::Finder ds{file_stream};
  auto const& res{ds.find(pattern)};

  std::cout << res.size() << std::endl;
  for (auto const& e : res) {
    std::cout << e.second.line_num << " " << e.second.pos + 1 << " " << e.second.result << std::endl;
  }

  return EXIT_SUCCESS;
}
